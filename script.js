const apiUrl = 'http://localhost:8080/';
const button = document.querySelector('#btn-pesquisar');
const termoBusca = document.querySelector('#linhaOnibus');
const divs = document.querySelector('section div');
const funcaoBusca = document.querySelector('section div');

function get(endpoint){
    return fetch(apiUrl + endpoint, {
        method: 'get',
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function post(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function parseData(dados){

    // "cl": 1470,
    //     "lc": false,
    //     "lt": "3902",
    //     "sl": 1,
    //     "tl": 10,
    //     "tp": "CPTM GUAIANAZES",
    //     "ts": "CPTM JD. ROMANO"

    let i = 0;

    let linhaIda = dados[0];
    let linhaVolta = dados[0];

    // divs[0].innerHTML = linhaIda;

    funcaoBusca.innerHTML = `${linhaIda.tp} - ${linhaIda.ts}`;
}

function buscarLinha(){
   get(`onibus/linha/${termoBusca.value}`).then(parseData);
}

button.onclick = buscarLinha;

// button.onclick = () => {
//     
// }

// populateDivs();
// get('jogo').then(parseData);
